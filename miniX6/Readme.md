[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX6/) to see my miniX6! <br>
[Click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX6/sketch.js) to see the source code!
----

<img src="screenshot.png" width=1000>

<img src="screenshotCode.png" width=1000>

----

### Svend's miniX6

#### Which MiniX do you plan to rework? <br>
This week I have revisited my [miniX1](https://svendbay.gitlab.io/aesthetic_programming/miniX1/), which focused on demonstrating the concept of RGB-values on a screen. I chose this miniX because I thought it could give me a perspective of how far my programming experience has taken me, and also I knew its concept did not really have a critical aspect. 
<br>

#### What have you changed and why? <br>
I kept the image and background, but changed what was happening on the canvas behind the screen. New syntax has been implemented like arrays and conditional statements. Instead of colors it now draws its own code inside the screen. I have used my miniX1 as a template to conceptualize the relation between code and screen.
<br>

#### How would you demonstrate aesthetic programming in your work? <br>
I believe for this miniX6 it is not necessarily the visual aspect that is interesting, but the code and the concept of what the code does. Because the code draws its own code (which draws its own code and so on) it creates these meta layers of code which obscures the line between what is text and what is code. What gets compiled and computed, and what does not. I thought this was weird and interesting to work with, because the code of software is often intended to be hidden and not visual to the user. The aesthetic aspect of the program lies in how code, which is continuously embedded in the fabric of society and culture through technology, remains hidden behind a visual layer. The code usually does not want to draw attention upon itself, but in my miniX6 it does this by creating code, creating code which creates code.
<br>
![Alt Text](https://media.giphy.com/media/kOIbusN7fPnkk/giphy.gif)
<p><a href="https://giphy.com/gifs/inception-briefcase-kOIbusN7fPnkk">via GIPHY</a></p>
<br>

#### What does it mean by programming as a practice, or even as a method for design? <br>
Through working with this miniX6 and thinking with the code, it has helped me reflect upon how code is visual, but also hidden — text, but also code. Aesthetic programming as a method for design helps me reflect critically upon what implications technology has on society. It serves as a practice or a method for reflecting and critiquing through a material medium (given that you consider softwares materiality. <br>

Aesthetic programming becomes an important tool for getting a practical understanding of mechanisms of programming, while accessing the hidden layers of digital culture, critiquing, intervening, exploring alternatives. <br>

> “Such practical “knowing” also points to the practice of “doing thinking,” embracing a plurality of ways of working with programming to explore the set of relations between writing, coding and thinking to imagine, create and propose “alternatives.” - Aesthetic Programming: A Handbook of Software Studies p. 16
<br>

My work demonstrates critical making as its transparency shows the code of which it is computed, and its meta-layers articulate how the tools we use for programming in high level languages like javascript also consists of code. Code creating code.
<br>
![Alt Text](https://media.giphy.com/media/7pHTiZYbAoq40/giphy.gif)
<iframe><p><a href="https://giphy.com/gifs/inception-7pHTiZYbAoq40">via GIPHY</a></p>
<br>
This way critical making helped me move my miniX1 from just demonstrating a concept of programming to critically reflect upon the aesthetics of programming.

----
References <br>
- Soon Winnie & Cox, Geoff, "Preface", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 12-24
- [GIPHY](https://giphy.com)
