### Transformation by Group 1, Julie, Christian, Jonas, Mads & Svend

----

#### 1. Technical Run-Through

<img src="transformation.jpg" width=600>


----

#### 2. Syntax


- `translate()` = creates new point of origin (0,0)
- `scale()` = scales the element on the x and/or y-axis
- `push()` = save
- `pop()` = restore (`push()` and `pop()` is always used together)
- `rotate()` = rotate the object around the origin (0,0) of the canvas

----

#### 3. Link with the curriculum and more


- **Wendy Chun:** "Transparency and Intransparency of software"

    “The computer - that most nonvisual and nontransparent device - has paradoxically <br> fostered “visual culture” and “transparency”.

    - `scale()` can create the 'illusion' of zooming in.
    - `scale()` can create depth/3D 'illusion' of an element coming closer.
    - `rotate()`creates the 'illusion' of spinning (when in `draw()`).
    - Difference between what happens visually and in the code.
    <br>

- **Anette Vee:** "new ways of thinking"? 
    <br>
    -   Making coding tangible
    <br>
- **Modifying the Universal:** 
    
    - The syntaxes as tools for modifying the existing
    


----

#### 4. Modifying the Universal

[Christians miniX2](https://christianrohde.gitlab.io/p5/miniX2/)

<img src="christian.png" width=600>

----



#### 5. Reflections



- Why has p5 named the syntax 'translate()'? Why not 'startPoint' or something?
- Why has p5 named the syntaxes 'push()' and 'pop()'?


----
