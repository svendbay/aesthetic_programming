function Dog(){
  this.x = 0;
  this.y = 0;
  this.xspeed = 1;
  this.yspeed = 0;
  this.total = 0;
  this.tail = [];

  this.dir = function(x,y){
    this.xspeed = x;
    this.yspeed = y;
  }

  this.death = function(){
    for (let i = 0; i < this.tail.length; i++){
      let pos = this.tail[i];
      let d = dist(this.x,this.y,pos.x,pos.y);
      if (d<1){
        noLoop();
      }
    }
  }

  this.update = function(){
    if (this.total === this.tail.length) {
      for (let i=0;i<this.tail.length-1;i++){
      this.tail[i]=this.tail[i+1];
      }
    }
    this.tail[this.total-1]=createVector(this.x,this.y);

    this.x=this.x+this.xspeed*(scl/1.5);
    this.y=this.y+this.yspeed*(scl/1.5);


    if (this.x > width-scl){
      this.x = 0;
    } else if (this.x < 0){
      this.x = width-scl
    } else if (this.y > height-scl){
      this.y = 0;
    } else if (this.y < 0){
      this.y = height-scl
    }


  }

  this.show = function(){
    fill(10);
    for (let i=0;i<this.total;i++){
      image(vaksbenPic,this.tail[i].x-0.2*scl,this.tail[i].y,scl*3,scl*3);
    }
    image(vaksPic,this.x,this.y,scl*3,scl*3);




  }
  this.eat = function(pos){
    let d = dist(this.x,this.y,pos.x,pos.y);
    if ( d < 0.5*scl){
      this.total++;
      woof.play();
      return true;
    } else {
      return false;
    }
  }
}
