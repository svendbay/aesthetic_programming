//https://www.youtube.com/watch?v=AaGK-fj-BAM
let playButton
let instructions
let start = 0;
let s;
let scl = 40;
let food;
let foodPic
let vaksPic
let vaksbenPic
let woof;
let col, rows;


function preload(){
  foodPic = loadImage('leverpostej.png');
  vaksPic = loadImage('vaks.png');
  vaksbenPic = loadImage('vaksben.png');
  woof = loadSound('woof.mp3');
}

function setup() {
  createCanvas(750,750);
  dog = new Dog();
  frameRate(10);
  imageMode(CENTER);
  pickLocation();
  col = width/scl
  rows = height/scl

  playButton = select('#play');
  playButton.position(width/2,height/2);
  playButton.size(100,25);
  playButton.style(CENTER);
  playButton.mouseClicked(startGame);

  instructions = select('#instructions');
  instructions.position(width/2-80,height/2+20);
  instructions.style('color','#fff');


}

function startGame(){
  start = 1;
  playButton.hide();
  instructions.hide();

}

function pickLocation(){
  var cols = floor(width/scl);
  var rows = floor(height/scl);
  food = createVector(floor(random(cols)),floor(random(rows)));
  food.mult(scl);

}

function draw() {
  drawBackground();

if (start == 1){
  dog.update();
  dog.show();
  dog.death();


  if (dog.eat(food)){
    pickLocation();
  }

  //fill(255,0,100);
  image(foodPic,food.x,food.y,scl*1.3,scl*1.3);
  }
  textSize(1*scl);
  fill(255);

  text('score: '+dog.total,1*scl,height-(1*scl));
}


function drawBackground(){
  background(10,175,50);

  for (let i=0; i<col; i++){
    for (let j=0; j<rows;j=j+2){
      textSize(1*scl);
      text('🌲', i*scl, j*scl);
    }
  }
  textSize(2*scl);
  text('🏠',1*scl,2*scl);

}

function keyPressed(){
  if (keyCode === 87 && dog.yspeed !== 1 ){ //UP
    dog.dir(0,-1);
  } else if (keyCode === 83 && dog.yspeed !== -1){ //DOWN
    dog.dir(0,1);
  } else if (keyCode === 65 && dog.xspeed !== 1){ //LEFT
    dog.dir(-1,0);
  } else if (keyCode === 68 && dog.xspeed !== -1){ //RIGHT
    dog.dir(1,0)
  }
}
