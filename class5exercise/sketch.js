let x = 0;
let y = 0;
let spacing = 10;
let letters = ['a','b','c']
let font = ['GEORGIA','HELVETICA']


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
}

function draw() {
  fill(255);
  if (random(1) < 0.7) {
    textFont(random(font))
    text(random(letters),x,y);
  }
  x+=10;
  if (x > width) {
    x = 0;
    y += spacing;
  }
}
