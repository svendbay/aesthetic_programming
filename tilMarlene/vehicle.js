function Vehicle(x,y){
  this.start = createVector(random(width),random(height/1.2))
  this.pos = createVector(this.start.x,this.start.y);
  this.target = createVector(x,y)
  this.vel = p5.Vector.random2D();
  this.acc = createVector();
  this.r=random(3,6);
  this.maxspeed = 20;
  this.maxforce = 5;

}

Vehicle.prototype.behaviors = function(){
  let arrive = this.arrive(this.target);
  this.applyForce(arrive);

}
Vehicle.prototype.spread = function(){
  let depart = this.depart(this.start);
  if (on){
    this.applyForce(depart);
  }
}

Vehicle.prototype.applyForce = function(f){
  this.acc.add(f);
}

Vehicle.prototype.update = function (){
  this.pos.add(this.vel);
  this.vel.add(this.acc);
  //basics physics idea: acc changing vel, vel changing pos
  this.acc.mult(0);
}

Vehicle.prototype.show = function(){
  stroke(255);
  strokeWeight(this.r);
  point(this.pos.x,this.pos.y);
}

Vehicle.prototype.shine = function(){
  this.r=constrain(this.r+sin(frameCount),3,6);
}

Vehicle.prototype.arrive = function(target){
  let desired = p5.Vector.sub(target,this.pos);
  let d = desired.mag();
  let speed = this.maxspeed;
  if (d<100){
    speed = map(d,0,100,0,this.maxspeed);
  }
  desired.setMag(speed);
  let steer = p5.Vector.sub(desired,this.vel);
  steer.limit(this.maxforce);
  return steer;
}
Vehicle.prototype.depart = function(start){
  let desired = p5.Vector.sub(start,this.pos);
  let d = desired.mag();
  let speed = this.maxspeed;
  if (d<100){
    speed = map(d,0,100,0,this.maxspeed);
  }
  desired.setMag(speed);
  let steer = p5.Vector.sub(desired,this.vel);
  steer.limit(this.maxforce);
  return steer;
}
