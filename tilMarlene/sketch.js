let font;
let vehicles = [];
let word = 'du er sød';
let on = false;

function preload(){
  font=loadFont('ProdigySans-Medium.otf');
}

function setup(){
  createCanvas(windowWidth,windowHeight);

  textAlign(CENTER);
  textFont(font);
  textSize(160);
  fill(255);
  noStroke();

  let points = font.textToPoints(word,0,200); //returns x,y and a for as an array of objects

  for (let i=0;i<points.length;i++){
    let pt=points[i];
    let vehicle = new Vehicle(pt.x,pt.y);
    vehicles.push(vehicle);
  }
}

function draw(){
  drawBackground();
  drawMoon();
  stars();

}

function mouseClicked(){
    on = !on
  }

function drawBackground(){
  let depth = windowHeight;
  push();
  colorMode(HSB);
  background(250,100,20);

  for (let i = 0; i<height/2;i++){
    let light = map(depth,0,height/2,190,250);
    strokeWeight(4);
    stroke(light,100,20);
    line(0,depth,width,depth);
    depth--;
    }
  pop();
}

function drawMoon(){
  fill(255);
  noStroke();
  ellipse(width-40,40,50);
  fill(8,0,51);
  ellipse(width-30,30,50);

}

function stars(){
  for (let i=0; i<vehicles.length;i++){
    let v=vehicles[i];
    v.behaviors();
    v.spread();
    v.update();
    v.show();
  }
  vehicles[int(random(0,vehicles.length))].shine();
}
