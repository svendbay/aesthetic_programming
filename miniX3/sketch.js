let countOne = ["one","two","three","four"];
let countTwo = ["one","two","three","four","five"];
let indexOne = 0; // index for counting to four
let indexTwo = 0; // index for counting to five
let flashInner = 50; //the defualt alpha value of the red and yellow ellipses
let flashOuter = 50;
let a = 0; // a = angle for the rotating needle
let speed = 1;// speed of the needle
let slider;
let fps; // frames per second (controls the tempo)
let monoSynth; // sound from p5 sound library

function setup() {
  // put setup code here
  createCanvas(windowWidth,windowHeight);
  slider = createSlider(15,60,30,1);
  slider.position(10, 500 );
  slider.style('width', '80px');
   monoSynth = new p5.MonoSynth();
}

function draw() {
  // put drawing code here
  background(30, 20, 120);
  let fps = slider.value();

  push() // the text for the tempo-slider
  stroke(175);
  strokeWeight(4);
  fill(255,0,0);
  textFont('Georgia');
  textSize(20);
  text('tempo',20,490)
  pop();
  frameRate(fps);

  drawText();
  drawElements();
  playRhythm();
  a = a+speed;
}

function drawText(){

  push();
  translate(width/2,height/2);
  textFont('Georgia');
  textAlign(CENTER);

  push();
  stroke(flashInner);
  strokeWeight(4);
  fill(255,255,0,flashInner);
  textSize(40)
  text('Poly',-60,-150);

  stroke(flashOuter);
  fill(255,0,0,flashOuter);
  textSize(40)
  text('Rhythm',50,-150);
  pop();

  //the text counting to four
  fill(255,255,0,flashInner);
  textSize(30);
  text(countOne[indexOne],0,20);

  //the text counting to five
  fill(255,0,0, flashOuter);
  textSize(30);
  text(countTwo[indexTwo],0,-10);
  pop();
}

function drawElements(){
    translate(width/2,height/2);

// the two circles
    push()
    rotate(a/30);
    strokeWeight(6);
    line(0,-40,0,-110);
    noFill();
    ellipse(0,0,100,100);
    ellipse(0,0,200,200);
    pop();


  push();
  // the 4 inner beats
  noStroke();
    for (let i = 0; i < 4; i++){
      fill(255,255,0,flashInner);
      rotate(2*PI/4);
      ellipse(0,-50,20,20)
    }
  // the 5 outer beats
    for (let i = 0; i < 5; i++){
      fill(255,0,0,flashOuter);
      rotate(2*PI/5);
      ellipse(0,-100,20, 20)
    }
    pop();
}

function playRhythm(){
  userStartAudio();
  // these define the notes playing
  let beatStrongOne = 'F2';
  let beatWeakOne = 'F2';
  let beatStrongTwo = 'F3';
  let beatWeakTwo = 'F3';

// This flashes the yellow dots 4 times per round and counts to four
  if (a == 180/4*1){
    flashInner = 255;
    indexOne++;
    monoSynth.play(beatWeakOne,0.5);
  } else if (a == 180/4*2) {
    flashInner = 255;
    indexOne++;
    monoSynth.play(beatWeakOne,0.5);
  } else if (a == 180/4*3) {
    flashInner = 255;
    indexOne++;
    monoSynth.play(beatWeakOne,0.5);
  } else if (a == 180/4*4+7) {
    flashInner = 255;
    indexOne++;

    flashOuter = 255;
    indexTwo++;
    monoSynth.play(beatStrongOne,0.5);


    a = -3; //this resets the angle for the needle.
    /* the -3 value + the extra +7 in line 98 creates
    a difference of -10 when reseting the needle, which
    somehow makes it look smooth
    */
  } else {
    flashInner = 175;
  }

// This flashes the red dots 5 times per round and counts to five
  if (a == 180/5*1){
    flashOuter = 255;
    indexTwo++;
    monoSynth.play(beatWeakTwo,0.5);
    } else if (a == 180/5*2) {
    flashOuter = 255;
    indexTwo++;
    monoSynth.play(beatWeakTwo,0.5);
    } else if (a == 180/5*3) {
    flashOuter = 255;
    indexTwo++;
    monoSynth.play(beatWeakTwo,0.5);
    } else if (a == 180/5*4) {
    flashOuter = 255;
    indexTwo++;
    monoSynth.play(beatWeakTwo,0.5);
    } else if (a == 180/5*5+7) {
    flashOuter = 255;
    indexTwo++;
    } else {
    flashOuter = 175;
    }
  // these reset the count
  if (indexOne == countOne.length){
    indexOne=0;
  }
  if (indexTwo == countTwo.length){
    indexTwo=0;
  }
}

function windowResized(){
  resizeCanvas(windowWidth,windowHeight);
}
