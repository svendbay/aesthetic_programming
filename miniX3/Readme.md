[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX3/) to see my miniX3! 

----

#### Svend's miniX3

<img src="polyrhythm.png" width=600>

----

This week I chose to explore time in relation to rhythm while designing my throbber. 
<br>
Inspired by [John Varney’s alternative visualization of rhythm](https://www.youtube.com/watch?v=2UphAzryVpY) I decided to create a throbber which could animate a loop and also visualize a 5/4 rhythm. This 5/4 is what is known as an odd time signature or polyrhythm within music. Its oddness stems from the fact that you count to 4 and 5 simultaneously. Rhythm is experienced as the relation between the beats and the loop, which I think relates very well to Lammerant quote.
<br>
<br>
 
> “Any observing or experience of difference, of presence related to an earlier or later absence, is linked with an experience of time.” <br>  Hans Lammerant in How humans and machines negotiate experience of time
<br>
This gives a special experience of time or time feel. Try looking at the yellow text in the throbber and count along, and afterwards try counting along with the red text. Counting along to the text tunes your internal clock to the rhythm. This odd time signature is used in [Dave Brubeck Quartet’s Take Five](https://www.youtube.com/watch?v=vmDDOFXSgAs). Furthermore, the tempo-slider allows for experiencing setting the tempo of the loop. Consider how difference in tempo affects the experience of time. The fast tempo makes the experience of time rushed or stressful and the slow tempo makes it relaxed or even incoherently dull. 
<br>
<br>
`For loops` to create the colored ellipses on the throbber.
`Push()` and `pop()` to isolate the attributes on the text.
`Arrays` to create the words counting inside the throbber.
`If else` conditional statements to make rhythm at the right time.
`monoSynth()` to add sound to the rhythm.
`createSlider()` to create the tempo-slider.
I have used custom functions `drawElements`,`drawText` `playRhythm` and `windowResized` to better divide and maintain an overview of the code. This worked really well. (also I don't know why this `` markdown does not work)
<br>
<br>
For reflecting on what a throbber communicates or refrains from communicating I have chosen the throbber from Det Kongelige Bibliotek’s webside when signing in. This throbber animates a motion from left to right which creates a linear time feel even though the animation is looping. This is different from most throbber which have a cyclical temporal expression. I find this throbber frustrating because it communicates the process moving backwards, as the motion is against the reading direction. This way the throbber hides what is actually happening in an unfortunate manner.  However this movement in the reading direction is culturally bound and someone reading from right to left might find it less frustrating.
<br>

<img src="kgl.png" width300>
the throbber from Det Kongelige Bibliotek

----

References:
<br>
[John Varney’s alternative visualization of rhythm](https://www.youtube.com/watch?v=2UphAzryVpY)
<br>
[Dave Brubeck Quartet’s Take Five](https://www.youtube.com/watch?v=vmDDOFXSgAs)
<br>
[Det Kongelige Biblioteks webside](https://www.kb.dk) (login to see the throbber)




