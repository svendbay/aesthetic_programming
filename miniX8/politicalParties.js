class Democrat_fact{
  constructor(political){
    this.speed = 4;
    this.pos = new createVector(50,900);
    this.political=political;

  }
  move(){
    this.pos.y-=this.speed
  }
  show(){
    let bounds = font.textBounds(democrat.posted[this.political].statement,this.pos.x,this.pos.y);
    noStroke();
    fill('blue');
    rect(bounds.x,bounds.y,bounds.w+30,bounds.h+20,20);
    fill(255);
    textFont();
    textSize(24);
    text(democrat.posted[this.political].statement,this.pos.x+15,this.pos.y+10);
  }
}

class Republican_fact{
  constructor(political){
    this.speed = 4;
    this.pos = new createVector(width-80,900);
    this.political = political
}
    move(){
  this.pos.y-=this.speed
  }
    show(){
      let bounds = font.textBounds(republican.posted[this.political].statement,this.pos.x,this.pos.y);
      noStroke();
      fill('red')
      rect(bounds.x-bounds.w,bounds.y,bounds.w+30,bounds.h+20,20);
      fill(255);
      textFont();
      textSize(24);
      text(republican.posted[this.political].statement,this.pos.x+15-bounds.w,this.pos.y+10);
  }
}
