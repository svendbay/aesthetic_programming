let republican, democrat
let political;

let myFacts=true;
let yourFacts=false;

let keyboard;
let toolbar;
let sound;
let font;
let polarized = true;


let democrat_fact = [];
let republican_fact = [];
let min_text = 6;


function preload(){
  republican = loadJSON('republican.json');
  democrat = loadJSON('democrat.json');
  keyboard = loadImage('keyboard_image.png');
  toolbar = loadImage('toolbar.png');
  sound = loadSound('facebook_chat_sound.mp3');
  font = loadFont('ProdigySans-Medium.otf');
}

function setup() {
  createCanvas(1000,1400);

}
function draw(){
  background(255);
  democracy();
}

function democracy() {

  if(myFacts == yourFacts){
    console.log("democracy successful")

  } else {

  checkText();
  timer();

  for(let t = 0; t<republican_fact.length; t++){
    republican_fact[t].show();
  }
  for(let i = 0; i<democrat_fact.length; i++){
    democrat_fact[i].show();
  }
  drawPhone();
  }
}
  function timer(){
    if(frameCount%300<30){ //execute for 1 sec every 5 sec
      moveText();
    }
    if(frameCount%300===1){ //execute 1 time every 5 sec
      claimStatement();
    }
  }

  function claimStatement() {
    polarized = !polarized
    political=int(random(0,9.1));
    if (republican_fact.length < min_text && !polarized) {
      republican_fact.push(new Republican_fact(political));
      sound.play(0.5);

      }
    if (democrat_fact.length < min_text && polarized) {
        democrat_fact.push(new Democrat_fact(political));
        sound.play(0.5);
      }

  }

  function moveText(){
    for(let t = 0; t<republican_fact.length; t++){
      republican_fact[t].move();

    }
    for(let i = 0; i<democrat_fact.length; i++){
      democrat_fact[i].move();
    }
  }

  function checkText(){
    for (let i = 0; i < republican_fact.length; i++){
      if(republican_fact[i].pos.y < -50){
        republican_fact.splice(i,1);
      }
    }
    for (let i = 0; i < democrat_fact.length; i++){
      if(democrat_fact[i].pos.y < -50){
        democrat_fact.splice(i,1);
      }
    }
}

function drawPhone(){
  image(keyboard,30,950,940,350)
  image(toolbar,30,910,940,70);
  stroke(0);
  noFill();
  rect(10,10,980,1380,20);
  rect(30,40,940,1250);

  push();
  fill(0);
  rect(10,1290,980,100,20,20);
  rect(10,10,20,1300,20);
  rect(970,10,20,1300,20);
  rect(10,0,980,40,20);
  pop();

  ellipseMode(CENTER);
  stroke(200);
  strokeWeight(4);
  noFill();
  ellipse(500,1335,50);

  push();
  stroke(150);
  fill(150);
  textSize(20);
  strokeWeight(1);
  text('Aa',400,950);
  pop();

  let act = ['','|']
  let now = second()%2;
  stroke(87, 176, 255);
  textSize(30);
  text(act[now],390,955);

}
