[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX9/) to our miniX9 <br>
[Click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX9/sketch.js) to see our source code


## MiniX9 - API's aginst humanities - Julie and Svend

<img src ='screenshot.png' width=1000>

<br>

##### What is the program about? Which API have you used and why?
Drawing on contemporary meme-culture and the popular card game “Cards Against Humanity” this program is about using data from API’s to create memes based on the given context. Your task is to create memes based on the given text and making a query giphy’s database for the right gif. However the result of the query is filtered as the user input will always be constrained to contain ‘kitty’. This is meant to reflect how the process of querying an API is often biased by socio-economic circumstances. 
<br>

##### Can you describe and reflect on your process in this miniX in terms of acquiring, processing, using, and representing data?
Being somewhat on the same page regarding this miniX assignment, the initial process of working out the idea and choice of API went fairly easily. Furthermore, since we landed on using the giphy api in our project which turned out to be very accessible. Giphy provides three options for using their api by access with public beta key, a beta key or a production key for developers to for example use in apps they intend to launch to a wider audience. We chose to work with the public beta key. To gain access you are not required to sign in with an account and we found the search restrictions reasonable. 
We reflected in hindsight on our choice of API, since we did consider other options. Why Giphy? We discussed how we’d both naturally gravitated to something we felt a relation to. As the designers of our program, to work with “a material” we felt connected to. And this likely due to the fact that we have come to use gifs as an extension for emojis and a way of expressing ourselves digitally. 
<br>

##### How much do you understand this data or what do you want to know more about? <br> – Try to formulate a question in relation to web APIs or querying/parsing processes that you would like to investigate further if you had more time.
We understand how we are able to examine the structure of the requested data. For instance, this revealed to us that there is a name-value pair for each object in the data-array called “rating” which rates the content of the gif from “G”-”pg”-”pg-13”-R”. However we could not find any R-rated content, and have theorized about whether the public beta key, which we are using, is restricted. 
We were also curious about the datafication of the gifs “trending_datetime”-value. Consider for instance the date on these two objects:
We would like to know more about how endpoints work and how to phrase these correctly. This could be interesting to work with as it would enable us to filter the query further.

 - We would like to know more about their two types of endpoints. ‘Trending’ and ‘Search’. <br>
- Is giphy still able to track our usage with the public beta key? How does this affect the power relation? <br>

##### How do platform providers sort the data and give you the requested data? 
Giphy sorts their given data in a data array with the gifs as objects. Furthermore each gif is subdivided into different edits of the gif inside the object “images”. For instance there is “original”, “downsized”, “fixed_height” etc. This enabled us to display any gif in a fixed height on the sketch.
<br>

##### What are the power relations in the chosen APIs? 
Despite it being user content, in the end Giphy has the final say in the content that is displayed through their services creating an asymmetrical power relation. In our case, we were restricted to content modified by a rating system provided by Giphy. 
<br>

##### What is the significance of APIs in digital culture?
In Snodgrass and Soons API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange they refer to Helmond’s description of API’s as an “underlying technological glue of the social web”. This can be understood as a description of how API’s enables different applications and platforms to become interconnected. But it is important to note that this ‘glue metaphor’ only represents some of the functional aspects of API’s and does not depict the sociotechnical aspect of API’s. We are tempted to claim the shortcoming of this metaphor, as glue can be understood as merely functional and neutral. <br>

For instance, the demand of using an API key enables cooperations to track our usage of their search engine. In this sense we pay for the service with our datafied behavior instead of actual money. Furthermore, we need to remember that when querying for data we are doing it on their premises. This means that through the structure of their data presentation, they determine what gets seen and what does not. Relating this to our Software Studies course, Matthew Fuller depicts an interesting example of how search engines as a software-based sociocultural phenomenon affects our notion of knowledge. “When the search engine becomes the primary basis of scholarly research, the keyword becomes a crucial pivot for the articulation of fields.”  (Fuller p. 59). Based on this example, we can see that while API’s can be described as ‘glue of the web’, the implications of the socio-technical structure of these, is beyond the web.


##### References
- https://giphy.com
- https://giphy.api-docs.io/1.0/welcome/access-and-api-keys
- Fuller, Matthew. "Software Studies Methods." How to be a Geek: Essays on the Culture of Software, edited by Matthew Fuller. John Wiley & Sons, 2017, 55-68. 
- Eric Snodgrass and Winnie Soon, “API practices and paradigms: Exploring theprotocological parameters of APIs as key facilitators of sociotechnical forms of exchange],”First Monday 24, no.2 (2019)
