let api = "https://api.giphy.com/v1/gifs/search?";
let api_key = "&api_key=dc6zaTOxFJmzC";
let query = "&q=";
let constraint = "-kitty";
let input;
let meme;
let memeFile;
let n;
// the full url (without input): https://api.giphy.com/v1/gifs/search?&api_key=dc6zaTOxFJmzC&q=
function preload(){
  bgImage = loadImage('memeBackground.png');
  memeFile = loadJSON('cards.json')
  font = loadFont('impact.ttf');
}

function setup() {
  createCanvas(windowWidth,windowHeight);
  background(bgImage);

  n = int(random(0,memeFile.text.length)); //chooses random new meme text when page reloaded

  push()
  rectMode(CENTER);
  fill(100);
  noStroke();
  rect(width/2,height/2,800,500);
  pop();

  let button = select('#submit'); //communicates with the index.html file
  button.position(width/2+10,height/2+200, 65);
  button.mousePressed(getGiphy);

  input = select('#word'); //communicates with the index.html file
  input.center()
  input.position(width/2-100,height/2+200);
   input.size(100);

   textFont(font);
   textSize(28);
   textAlign(CENTER);
   fill(255);
   stroke(0);
   text(memeFile.text[n]+"_____",width/2,height/2-200);

   push();
   fill(150);
   textSize(20);
   text("Complete the sentece", width/2-20,height/2+140)
   pop();
}

function getGiphy(){
  let url = api + api_key + query + input.value() + constraint;
  loadJSON(url,gotData);
}

function gotData(giphy) {
  removeElements();

  push()
  rectMode(CENTER);
  fill(100);
  noStroke();
  rect(width/2,height/2,800,500);
  pop();

  meme = createImg(giphy.data[0].images.fixed_height.url);
  if (meme.width===0){ //fixes a bug where p5 can't find the width of the gif and returns 0.
    meme.width=270;
    meme.height=200;
  }
  meme.position(width/2-meme.width/2,height/2-meme.height/2);

  textFont(font);
  textSize(28);
  textAlign(CENTER);
  fill(255);
  stroke(0);
  text(memeFile.text[n]+" "+input.value(),width/2,height/2-200);
}
