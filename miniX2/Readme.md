[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX2/) to see my see my miniX2! <br>
Note: I recommend using firefox or chrome, as safari by default blocks audio from unknown webpages. If you are having trouble with this [click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX2/HowToAllowAudio.md).
<br>
Also note: If the music become a bit to 'funky', please refresh the page, as the tracks might be out of sync. 

----
<img src="emojion.png" width=600>

----
### Svend's miniX2
----
##### Describe your program and what you have used and learnt. 

For this weeks miniX I have decided to work with emojis and music to reflect on emojis ability to express human emotion. The program consists in its initial state of three white circles on a black canvas. The circles are placed equally on the x-axis and equally distanced from each other. This creates are very balanced, but numb expression. When you click on the circle the image shifts to an emoji with an instrument, and you are able to hear the instrument playing. Each emoji has its own instrument and allow for combining different combinations or compositions of the music. When clicked again the emoji shifts back to a circle and the instrument stops playing.

<br>
<img src="emojioff.png" width=600>
<br>

On the code-level the program consists of three .jpeg-images of the same circle on a `background()` set to 0. I originally wanted to use p5’s `ellipse()`-syntax, but I had much difficulty using the `.mouseClicked()`-syntax (which I will use later) with the ellipse. Three .mp3-tracks are preloaded with `preload()` along with image three images of an emoji playing an instrument ((line 6-15).  These tracks are set to `loop()` in the setup function, but at 0 volume (line 24-31). It is important that these tracks are always playing even when you can’t hear them, so that the tracks are synced when the volume goes up. Next up, the emojies are set up with `createImg()` and supplied with a `.mouseClicked()` (line 34-50).  This checks for whenever the mouse is clicked on that specific image, which initiates a custom function called `playDrums()`.  This custom function checks the boolean statement `volDrums` (line 54-62), which is by default set to`false` (line 2). If it is `false`, the function sets it to `true` which set sets the volume to 1 and displays in the console log that the drums are playing (line 61-61). I used this `console.log`, while exploring the code, to be able check whether the function worked, without having to take account for `setVolume()` or the image of the emoji playing drums which is drawn on top of the circle when `playDrums()` is run (line 64-67). The `imageDrums` emoji has `.mouseClicked()` which activates a second custom function `hideDrums()` (line 70). This function uses `hide()` to remove `imageDrums`, which reveals makes the circle underneath disposable for `.mouseClicked()` again. This also triggers `playDrums()` so that the `volDrums` can be reset to `false` and turn the volume to 0. This whole sequence for turning the drums on/off (from line 54-74) is repeated for the two other instruments

During this week I have learned about interaction with elements in javescript. I learned that there are multiple ways of creating the same interaction with these elements, which may be experienced similarly, but are different on the code-level. I was inspirered to work with interaction elements by [Sine Barslunds miniX1](https://gitlab.com/sine.bj44/aesthetic-programming/-/tree/master/MiniX1). The concept of combining different tracks comes from an old game about [singing horses](http://www.dennyweb.com/singing_horses.htm) I remember playing when I was younger.  I also learned some of the syntax used for working with the p5.sound library.  

----
##### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?
<br>
The reason why I chose to make my emojies like this was to explore the emojis ability to express emotion. According to Executing Practices (p. 35) emojis primary function, before they began serving as means of self-representation, was to communicate emotion. This made me reflect on my relation to emojis and I often refrain from using them. I argue that they are limited in their ability to communicate human emotion, because they oversimplify concepts of love, anger, sadness etc. On the other hand music is a concept that I believe deals very well with expressing the complexity of human emotion. I asked the question _what would happen if I combined the simplistic representation of emojis with musics ability to express human emotion palette?_. Consider, for instance, how you percieve the emoji having the sound off versus having it on? Furthermore, I believe my emojis fit into the cultural context of Google's attempt to design emojis that refrain from colonistic connotions or representation of gender, identity or race as mentioned in Executing Practices (p. 41). The template from which my "universal charachter" (p. 42) stems from is the circle, as expressed by the programs defualt setting. Are they still relatable? Perhaps the musical element will make them relatable, perhaps it will not.
----

##### References

[Daniel Shiffman 7.8, Objects and Images](https://www.youtube.com/watch?v=i2C1hrJMwz0&t=263s)
<br>

[Sine Barslunds miniX1](https://gitlab.com/sine.bj44/aesthetic-programming/-/tree/master/MiniX1)
<br>

[Singing Horses](http://www.dennyweb.com/singing_horses.htm)
<br>

Roel Roscam Abbing, Peggy Pierrot and Femke Snelting, “Modifying the Universal,” in Helen Pritchard, Eric Snodgrass & Magda Tyżlik-Carver, eds., Executing Practices (London: Open Humanities Press, 2018), 35-51, http://www.data-browser.net/db06.html.

----




