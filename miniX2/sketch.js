let emoji, drums, bass, keys, imageDrums, imageBass, imageKeys;
let volDrums = false;
let volBass = false;
let volKeys = false;

function preload() {
  //loading sound files
  drums = loadSound("drums_miniX2.mp3");
  bass = loadSound("bass_miniX2.mp3");
  keys = loadSound("keys_miniX2.mp3");

  //loading images
  imageDrums = loadImage("imageDrums.jpeg");
  imageBass = loadImage("imageBass.jpeg");
  imageKeys = loadImage("imageKeys.jpeg");
}

function setup() {
  // put setup code here
  createCanvas(600, 500);
  background(0);

// loops the soundfiles, but sets the volume to 0
drums.loop();
drums.setVolume(0);

bass.loop();
bass.setVolume(0);

keys.loop();
keys.setVolume(0);

// first circle
emoji = createImg("emoji.jpeg");
emoji.position(80,200);
emoji.style("width","100px");
emoji.mouseClicked(playDrums);

// second circle
emoji = createImg("emoji.jpeg");
emoji.position(240,200);
emoji.style("width","100px");
emoji.mouseClicked(playBass);


// third circle
emoji = createImg("emoji.jpeg");
emoji.position(400,200);
emoji.style("width","100px");
emoji.mouseClicked(playKeys);

// The next three sections are the same, but applied to each circle
// turns vol on/off for drums
function playDrums(){
  if (volDrums){
    volDrums = false;
    console.log("Drums playing? ="+volDrums);
    drums.setVolume(0);
  } else {
    volDrums = true;
    console.log("Drums playing? ="+volDrums);
    drums.setVolume(1);
    //also shows drums emoji
    imageDrums = createImg("imageDrums.jpeg");
    imageDrums.position(80,180);
    imageDrums.style("width","160px");
    imageDrums.mouseClicked(hideDrums);
  }
}
function hideDrums (){
  //hides the drums emoji activates playDrums-function
  imageDrums.hide();
  playDrums();
}

// turns vol on/off for bass
function playBass(){
  if (volBass){
    volBass = false;
    console.log("Bass playing? ="+volBass);
    bass.setVolume(0);
  } else {
    volBass = true;
    console.log("Bass playing? ="+volBass)
    bass.setVolume(1);
    //also shows bass emoji
    imageBass = createImg("imageBass.jpeg");
    imageBass.position(240,165);
    imageBass.style("width","130px");
    imageBass.mouseClicked(hideBass);
  }
}
function hideBass (){
  //hides the bass emoji activates playBass-function
  imageBass.hide();
  playBass();
}

// turns vol on/off for keys
function playKeys(){
  if (volKeys){
    volKeys = false;
    console.log("Keys playing? ="+volKeys);
    keys.setVolume(0);
  } else {
    volKeys = true;
    console.log("Keys playing? ="+volKeys)
    keys.setVolume(1);
    //also shows keys emoji
    imageKeys = createImg("imageKeys.jpeg");
    imageKeys.position(400,190);
    imageKeys.style("width","160px");
    imageKeys.mouseClicked(hideKeys);
  }
}
function hideKeys (){
  //hides the keys emoji activates playKeys-function
  imageKeys.hide();
  playKeys();
  }
}
