### miniX10 - Algorithms and flowcharts

<img src="flowchart1.png" width=1000> <br>
<img src="flowchart2.png" width=1000> <br>
<img src="flowchart3.png" width=1000> <br>

----

This week I chose to work on my [miniX7](https://gitlab.com/svendbay/aesthetic_programming/-/tree/master/miniX7) (about OOP) for the individual flowchart. I was a bit uncertain as to what miniX to choose, because ‘the most complex’ can refer to different kinds of complexity. However I chose this one because it features a lot of complexity in the relation between objects and contains a lot of conditional structures.
<br>

#### What are the difficulties involved in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level? <br>
What I found difficult was while making the individual flowchart was that I had to decide on a level of abstraction. To be able to explain the program to a non-coder I chose to mainly focus on the visual. For instance, I called the conditional statement for collision between shark and fish for “shark hit fish?”, even though this event is actually triggered by the distance between the objects. I thought this descriptive abstraction of the algorithmic process aligned very well Ensmengers description of flowchart: _“like all maps, the flowchart represented only a selective perspective on reality.” (Ensmenger, p. 346). _This selective perspective on reality means that I could have broken all the processes down right until 1’s and 0’s, but I have chosen to keep it on a higher level of abstraction to make it easier to comprehend the complexity going on at the algorithmic procedural level. This may mean that essentially the flowchart is wrong, but nevertheless comprehendible.
<br>

#### What are the technical challenges facing the two ideas and how are you going to address these? <br>
The Personality-test: <br>
The technical challenge if we choose this idea is designing a value system for the algorithm to follow. For each question we need to assign a value to each answer. Furthermore, the waiting game while processing the test-results needs to be implemented. This program contains many different states and phases, which I believe, is something we have not attempted before. To complete these challenges we must divide and conquer so some group members are designing the value system, while others are designing the game.
<br>

The “feed it with your data”: <br>
The most challenging part for this idea is to program how the data will be captured. Do we structure questions about personal data in a JSON-file? Do we construct pop-up windows as different classes? This program also contains multiple elements which we as a group can separate and work on. Some might design the cats behavior, while other focus on the data capture. Perhaps someone will work with how this data is processed.
<br>

#### In which ways are the individual and the group flowcharts you produced useful? <br>
The individual flowchart which I have made a couple of weeks after writing the program, is useful for technical description. This flowchart simply maps out an already existing and logically coherent process. I chose to work in Axure RP 9, because this is a useful tool with shapes designed specifically for flowcharts. It was my understanding that this flowchart would primarily be useful for explaining the flow of the program to others, but while making the flowchart I realized, that this also helped me comprehend the flow better. For instance, I wanted to structure the program in an object-oriented way, as the task for that week was object-oriented approaches to programming. This is reflected in how each object class is divided into separate sections (e.g. bubbles, sharks and fish). This also meant the sections had to be mapped out according to the objects relation to each other. Consider for instance how the shark is placed in the middle as this objects behavior is related to both the bubble objects and the fish objects. During the process of making the flowchart I also realized how similar the two endings of the game was. Both in the win- and lose-scenario the program displays text and freezes (noLoop;).
<br>

The collaborative flowchart served the purpose as a tool and a roadmap for planning the design process. As we chose to work in miroboard, to enable everyone to get hands-on engaged, this meant that we could use the flowchart to map out the relation between the different stages of the algorithmic process. 
<br>
> “It is as “triggers of contradictions and negotiation,” rather than as stable, mutually agreed upon representations of reality, that boundary objects help “explain the potentially conflictual nature of collaborative activity.” Ensmenger p. 346
<br>
For instance, the visual representation, made us think about whether the questions in the personality test should be determined by the answer to previous questions or not. This flowchart helped us agree upon which processes to include and it what order. However, as this served primarily as a tool for brainstorming the concept, we did not go into the technical details of the program (see question #2). By comparing my individual flowchart to the collaborative flowcharts you can see that the former is more technical, while the later draws up the overall concepts of the programs. I do not think this is bad however because it enables as this higher level of abstraction and “ill-defined-ness” enables us to modify the concept during the next stage of the design process.

This relates well to the notion from our Aesthetic Programming Handbook on the abstractness of algorithms: _“An algorithm differs from lines of code in that it is not dependent on specific software and libraries.” (Soon & Cox p. 213)._ We do not need to define the specific code or syntax yet as the abstract nature of our algorithm can so far be captured in the paths and rules of our flowchart. The  abstraction on these flowcharts, thus gives the flowcharts a pliability that fuels the iterative process of designing our final project.

----

Litterature:

- Nathan Ensmenger, “The Multiple Meanings of a Flowchart,” Information & Culture: AJournal of History 51, no.3 (2016): 321-351, Project MUSE, doi:10.1353/lac.2016.0013.

- Soon Winnie & Cox, Geoff, "Algorithmic procedures", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 211-226

Other references:

- [Axure RP 9](https://www.axure.com) 
- [Miroboard](https://miro.com)


