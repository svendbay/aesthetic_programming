[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX1/) to see my see my miniX1!
<br>
<img src="colourtv.png" width=600>
----
#### Svend's miniX1
This program is about showing the concept of 'Red, Green and Blue' (RGB) works. Within the colour-tv, the three different colours used for creating other colours, continously overlap in different combinations to create different colours. This rhythmic pattern is framed inside an image of an old TV to show that this concept is linked to how screens show color. 
<br>
<br>
The syntax used consists of creating the variable 'tv' (line 1) to place the image. Followed by a preload() function (line 3). This means that the .png is already loaded, when I place it on top of the canvas (line 32-33). The canvas is also defined by 'let' (line 9), although I am not sure what the difference is between 'let' and 'var'. Furthermore, the moving ellipses are created by the draw() function (line 16). As the draw() is set to draw every 0.5/fps instead of 60/fps (line 13), the ellipses move to a random x and y coordinate every 2 seconds. The ellipses divided into the three colours by giving them the max value in the paramater of their colour (red ellipse = (255,0,0)). I have attempted to illustrate this by commenting the colour in the code '//' (see line 19). The ellipsis are also drawn with the attribute of being someone transparent, which is encoded in the fourth parameter ("alpha") of the fill() function. This is essentiel to the concept as it is this transparency on top of the white canvas that creates the mixture of colours.
<br>
<br>
I have learned a lot during my first week of coding. I have learned how color values work, how you can layer different objects on the canvas, how to load images. My source of inspiration came largely from exploring the reference p5 page and watching Shiffman (see the references). I also found that the best way to wrap my head around new syntax was to copy the code and mess around with it. This reminds me a lot about Donald A. Schön's term 'seeing-moving-seeing'. Programming is a new world for me and I find it to be quite captivating already. 
<br>
<br>
The difference between reading and writing code in my (shorthanded) experience is that while reading code allows for discovering new possibilities, writing code allows for creation and expression. However, they are go hand in hand in the sense that after having read code I can learn to write it, and after having written it I comprehend reading new code better. 
<br>
<br>
At large I believe that I subscribe to the notion of programming as "the new literacy", "the second literacy" etc. Programming is being able to express, control and design technology. But programming is also being able to logically understand the technological infrastructure of digital age. I also found  Anette Vee's notion of coding as mass literacy as both interesting and troubeling. Interesting in the sense that I conceptually find the writing of programming fundamentally different from traditional writing. For instance, could you write a novel expressed in javascript? I also find Anette Vee's notion troubling on a political level, because literacy is a social construction changing over time rendering the non-coders illiterate. That's scary.


----
References:
<br>
[Daniel Shiffman p5.js 1:4 Color](https://www.youtube.com/watch?v=9mucjcrhFcM&vl=zh)
<br>
[p5.js reference 'image()'](https://p5js.org/reference/#/p5/image)
<br>
[oldtv.png](https://www.stickpng.com/img/electronics/tvs/television-empty-vintage)
----
