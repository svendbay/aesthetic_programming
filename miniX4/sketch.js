let button;
let capture;
let ctracker;
let dots = ['','.','..','...'];
let scanTime = 0;
let maxScanTime = 100;
let result;
let clicks = 0;

function setup(){
  createCanvas(640,480);
  background(207,185,151);

  //pre-decides the result of the facescan
  result = floor(random(2));

  //video
  capture = createCapture(VIDEO);
  capture.size(640,480);
  capture.hide();

  //face ctracker
  ctracker = new clm.tracker();
  ctracker.init(pModel);
  ctracker.start(capture.elt);

}
function draw(){

  let s = second();
  let d = s%dots.length;
  let positions = ctracker.getCurrentPosition();


  image(capture,0,0,640,480);
  filter(GRAY);

  button = createButton('retry'+'('+clicks+')')
  button.position(10,10);
  button.style("color","#fff");
  button.style("background", "#ff9900");
  button.mousePressed(reset);



// initial scanning
  if(positions.length && scanTime<maxScanTime){

       stroke(255, 153, 0,200);
       strokeWeight(4)

       //top left
       line(positions[33][0]-50,positions[33][1]-100,positions[33][0]-100,positions[33][1]-100);
       line(positions[33][0]-100,positions[33][1]-100,positions[33][0]-100,positions[33][1]-50);

       //top right
       line(positions[33][0]+50,positions[33][1]-100,positions[33][0]+100,positions[33][1]-100);
       line(positions[33][0]+100,positions[33][1]-100,positions[33][0]+100,positions[33][1]-50);

       //bottom left
       line(positions[33][0]-50,positions[33][1]+100,positions[33][0]-100,positions[33][1]+100);
       line(positions[33][0]-100,positions[33][1]+100,positions[33][0]-100,positions[33][1]+50);

       //bottom right
       line(positions[33][0]+50,positions[33][1]+100,positions[33][0]+100,positions[33][1]+100);
       line(positions[33][0]+100,positions[33][1]+100,positions[33][0]+100,positions[33][1]+50);

       //text
       push();
       strokeWeight(1);
       fill(255, 153, 0,200);
       textAlign(CENTER);
       textFont('GEORGIA');
       textSize(20);
       text('scanning facial data'+dots[d],positions[33][0],positions[33][1]-120);
       pop();

       //counts the frames passed since facetracker run
       scanTime++;
       console.log(scanTime);

  } else if(positions.length && scanTime==maxScanTime && result==0){
    // BAD

      stroke(255,0,0,100);
       strokeWeight(4);

       //top left
       line(positions[33][0]-50,positions[33][1]-100,positions[33][0]-100,positions[33][1]-100);
       line(positions[33][0]-100,positions[33][1]-100,positions[33][0]-100,positions[33][1]-50);

       //top right
       line(positions[33][0]+50,positions[33][1]-100,positions[33][0]+100,positions[33][1]-100);
       line(positions[33][0]+100,positions[33][1]-100,positions[33][0]+100,positions[33][1]-50);

       //bottom left
       line(positions[33][0]-50,positions[33][1]+100,positions[33][0]-100,positions[33][1]+100);
       line(positions[33][0]-100,positions[33][1]+100,positions[33][0]-100,positions[33][1]+50);

       //bottom right
       line(positions[33][0]+50,positions[33][1]+100,positions[33][0]+100,positions[33][1]+100);
       line(positions[33][0]+100,positions[33][1]+100,positions[33][0]+100,positions[33][1]+50);

       push();
       strokeWeight(1);
       fill(255,0,0,100);
       textAlign(CENTER);
       textFont('GEORGIA');
       textSize(20);
       text('BAD',positions[33][0],positions[33][1]-120);
       pop();

     } else if(positions.length && scanTime==maxScanTime && result==1){
       //GOOD
       stroke(0,255,0,100);
       strokeWeight(4);

       //top left
       line(positions[33][0]-50,positions[33][1]-100,positions[33][0]-100,positions[33][1]-100);
       line(positions[33][0]-100,positions[33][1]-100,positions[33][0]-100,positions[33][1]-50);

       //top right
       line(positions[33][0]+50,positions[33][1]-100,positions[33][0]+100,positions[33][1]-100);
       line(positions[33][0]+100,positions[33][1]-100,positions[33][0]+100,positions[33][1]-50);

       //bottom left
       line(positions[33][0]-50,positions[33][1]+100,positions[33][0]-100,positions[33][1]+100);
       line(positions[33][0]-100,positions[33][1]+100,positions[33][0]-100,positions[33][1]+50);

       //bottom right
       line(positions[33][0]+50,positions[33][1]+100,positions[33][0]+100,positions[33][1]+100);
       line(positions[33][0]+100,positions[33][1]+100,positions[33][0]+100,positions[33][1]+50);

       //text
       push();
       strokeWeight(1);
       fill(0,255,0,100);
       textAlign(CENTER);
       textFont('GEORGIA');
       textSize(20);
       text('GOOD',positions[33][0],positions[33][1]-120);
       pop();
     } else if(!positions.length) {
       scanTime = 0;
     }
}
function reset(){
  //resets the result to 0 or 1
  result = floor(random(2));
  console.log(result);
  scanTime=0;
  clicks++;
}
