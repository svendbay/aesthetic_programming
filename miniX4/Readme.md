[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX4/) to see my miniX4!
[Click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX4/sketch.js) for source code!
----
<img src="screenshot.png" width=600>

----

#### Binary Judgement - Svend's miniX4

In a society where datafication advances and your data fuels survaillance capitalism, more and more responisbility is relies on datafication algorithms ability to predict extract, analyse and predict our behavior. This ability to control online behavior is impressive, but is it indeed the human or the computer that is in control? Binary Judgement expresses possible future(?) where technology decides our faith. The judgement of whether you are "BAD" or "GOOD" is left interely up to an intransparent algorithm running on a heartless machine. You have no influence on the outcome, except for asking for a second judgement. On the surface it seems to be analyzing your face extracting data, but is that really what is happening on the code-level? What future awaits us if we let machines running unnuanced on 0's and 1's make decisions based on datafication of human behavior? One must ask if survaillance capitalism leads to a technocratic dystopia.

In this program I have used the video capture and clmtracker library. Through working with this I really got my head around 2D-arrays to position the tracker points. I also worked with the modulo-operator, which was good as I had not understood its potential before this assignment. I have also worked with DOM-elements in the 'retry'-button, but I feel like this CSS-feature could have been explored further if I had had more time. Although working with video-capture was a lot of fun, I found it frustrating that it drained my CPU really hard. This was definetly a challenge as I had to work the non-video elements in a different sketch, to maintain a satisfying workflow.

My program relates the theme of "CAPTURE ALL" because it makes the computer capture it's sorroundings and using these data-inputs in conditional statements to compute according outputs. The binary aspect of the code is further emphasized in the title of the project and the binary judgement of either good or bad. This algorithm is of course extremely simplified in relation to the face-analysis and recognition software used in society, but this simplification is not necessarily visual, because the program is so intransparrent about the processing. There is also a datafication of the users attempts to appeal for a second judgement through the 'retry'-button, which displays number of times the button has been clicked. Through these elements, the program demonstrates multiple modes of capture.

----

References:

- “p5 DOM reference,” https://p5js.org/reference/#group-DOM.
- Shoshana Zuboff, “Shoshana Zuboff on Surveillance Capitalism | VPRO Documentary” https://youtu.be/hIXhnWUmMvw.
- Soon Winnie & Cox, Geoff, "Data capture", Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020, pp. 97-119

