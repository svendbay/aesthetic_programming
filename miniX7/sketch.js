let start = false;
let score = 0;

let shark;

let bubbleSound;
let bubbles = [];
let numBubbles = 40;

let fish = [];
let numFish = 30;
let eatFishSound = ['C4','D4','E4','F4','G4','A5','B5','C5'];
let n = 0;

function preload(){
  bubbleSound = loadSound("BubbleSound.m4a");
}

function setup(){
  createCanvas(windowWidth,windowHeight);

  for (let i=0;i<numBubbles;i++){
    let x = random(width);
    let y = random(height);
    let r = random(5,10);
    bubbles[i] = new Bubble(x,y,r);
  }

  for (let i=0;i<numFish;i++){
    let x = random(width);
    let y = random(height/1.3+10);
    let size = int(random(1,7));
    let c = random(100,200);
    let speed = 1;
    fish[i] = new Fish(x,y,size,c,speed);
  }

  shark = new Shark();

  monoSynth = new p5.MonoSynth();

}

function draw(){
  drawBackground();
  checkEating();
  checkWin();
  showText();
  playMusic();

  for (let i=0;i<bubbles.length;i++){
    bubbles[i].float();
    bubbles[i].show();
    bubbles[i].check();
  }
  for (let i=0;i<fish.length;i++){
    fish[i].swim();
    fish[i].show();
    fish[i].check();
  }
  shark.show();
  shark.move();
}

function drawBackground(){
  let depth = 0;
  push();
  colorMode(HSB);
  background(250,100,55);

  for (let i = 0; i<height/2;i++){
    let light = map(depth,0,height/2,190,250);
    strokeWeight(4);
    stroke(light,100,55);
    line(0,depth,width,depth);
    depth++;
    }
  pop();
}

function checkEating(){
  for (let i=0; i<fish.length;i++){
    let d = int(dist(shark.pos.x,shark.pos.y+5*shark.size,fish[i].x,fish[i].y)/shark.size);
      if (d<10 && shark.size >= fish[i].size){
        fish.splice(i,1);
        shark.size=shark.size+0.5;
        score++;
        monoSynth.play(eatFishSound[n]);
        n=n+1;
        if (n>eatFishSound.length-1){
          n=0;
        }
      } else if (d<10 && shark.size < fish[i].size){
        fill(255);
        textSize(30)
        text('YOU WERE EATEN',width/2,height-100);
        noLoop();
      }
  }
}
function checkWin(){
  if (fish.length==0){
    push();
    fill(255);
    textSize(30)
    textAlign(CENTER);
    text('Congratulations! You are at the top of the food chain!',width/2,height-100);
    noLoop();
    pop();
  }
}
function showText(){
  fill(255);
  textSize(20);
  text('shark-size: '+shark.size,10,height-30);
  text('score: '+score,10,height-10);

  if (start==false){
    push();
    textAlign(CENTER);
    text('Press arrow keys to eat fish that are smaller than you.\n But remember, there is always a bigger fish!',width/2,height-150)
    pop();
  }

}
function playMusic(){

}
