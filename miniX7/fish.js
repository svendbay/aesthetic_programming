class Fish {
  constructor(x,y,size,c,speed){
    this.x=x;
    this.y=y;
    this.size=size;
    this.c=c; //color
    this.speed=speed;
  }
  swim(){
    this.x=this.x+random(4)*this.speed;
    this.y=this.y+random(-1,1)*this.speed;
}
  show(){
    fill(255,this.c,120);
    ellipse(this.x,this.y,40*this.size,10*this.size);
    ellipse(this.x-20*this.speed*this.size,this.y,8*this.size,12*this.size);

    fill(0);
   ellipse(this.x+14*this.speed*this.size,this.y,4*this.size) //eye
  }
  check(){
    if(this.x>width+20*this.size || this.x<-20*this.size){
      this.speed=this.speed*-1
    }
  }
}
