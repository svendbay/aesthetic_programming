[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX7/) to see my miniX7! <br>
Click here to see source code for [sketch.js](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX7/sketch.js), [shark.js](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX7/shark.js), [fish.js](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX7/fish.js), and [bubble.js](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX7/bubble.js)! <br>
For best result, play in fullscreen

----

<img src="screenshotPlay.png" width=1000>

<img src="screenshotLose.png" width=1000>

<img src="screenshotBig.png" width=1000>

----

### Svend's miniX7

#### How does/do your game/game objects work? <br>
The rules of the game are simple: You control a small shark which can go up, down, right or left, and the shark must eat the fish which are swimming above you. However, the shark can only eat the fish that have a smaller size than itself and for every fish eaten, the shark will grow in size. Eat all the fish and you win. <br>

The game consists of three classes, or types of objects: `shark.js`, `fish.js`, and `bubble.js`: <br>

The shark has four properties: position, size, speed and direction. There is only one instantation of this object. The shark has two methods to the object. `Move()` which facilitates  the position and direction of this object can be controlled by the arrow keys. This method also plays the bubble sound and draws instantiations of bubble objects at the sharks position (so it looks like its swimming fast). The second method is `Show()` which draws the representation of the shark relative to the position, direction and size. <br>

The fish have four properties: position, size, color and speed with randomized values which means that the 30 instantiations will be diversified. This object has three methods `swim()` which moves the position, `show()` to draw the fish and `check()` to reverse the direction of `swim()` when they hit the edge of the canvas. <br>

The bubble only has two properties: position and radius. These are also random. This object also has three methods: `Float()` which decreases the value of the y-position so that it represents floating upwards. `Show()` draws the bubble relative to the position and `check()` removes the bubble from an array so that the program does not create to many instantiations to keep track of. This method also draws new bubbles when the previous ones have floated of the canvas.  <br>

The main sketch, which contains the logic of the game, has two functions that are important to the game. The function checkEating() checks how close the shark object is to a given fish object. This function determines the relation between the objects. If the shark is equal or larger than the fish, the shark eats the fish, the score goes up and the shark groes 0.5 in size. If the shark is smaller, the game program will do `noLoop()`. This means that the shark class has a privilege that the fish class does not; it’s ability to grow in size. <br>

This power relation between the two classes also demonstrates a fundamental concept in Object Oriented Programming; abstraction: _“(…) a paradigm of programming in which programs are organized around data, or objects, rather than functions and logic.”_ This binary possibility of outcome of a collision between two classes, based on the data carried in the object (e.g. its .size) is an abstraction of nature. In the real world the it is not always the size of the class that determines who “wins” (also fish don’t eat sharks). Thus the program reduces this complexity of nature, in the relation between the objects.<br>

#### Draw upon the assigned reading, are the characteristics of object-oriented programming and the wider implications of abstraction? <br>
By creating this abstraction of “survival of the fittest” I have compressed and reduced it into a simple little game which is easy to comprehend. According to Matthew Fuller and Andrew Goffey this is socialtechnical practice of Object Oriented Modelling the world is: <br>

> “(…) compressing and abstracting relations operative at different scales of realities, composing new forms of agency (…)” Matthew & Goffey p. 1

This means that by abstracting elements of reality not only affects our mental model of computational procedures, but also the territorializing powers of these computational objects. This means OOP’s ability to encapsulate data in objects and reuse code in class libraries is also what makes software less accessible and transparent. <br>

> “As a principle and as a technical constraint, encapsulation and the hiding of data at the very least gives shape to a technico-economic hierarchy in which the producers of programming languages can control the direction of innovation and change, by promoting ‘lock-in’ and structuring a division of work that encourages programmers to use proprietary class libraries rather than take the time to develop their own” Matthew & Goffey p. 11

This consequence of OOP emphasizes the relevance of aesthetic programming. By examining how OOP enables abstraction of the world it may also constrain this representation and bear invisible power relations within. Aesthetic programming can help expose these political aesthetics and reflect on their implications on society and culture. 

----
References:
<br>
- Matthew Fuller and Andrew Goffey, “The Obscure Objects of Object Orientation,” in MatthewFuller, How to be a Geek: Essays on the Culture of Software (Cambridge:Polity, 2017).

----

