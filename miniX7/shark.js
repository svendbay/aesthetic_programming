class Shark {
  constructor(){
    this.pos=new createVector(width/2,height-80); //x and y position
    this.size=1; //size
    this.speed=8; //speed
    this.dir=0; //direction
  }
  show(){

    push();
    translate(this.pos.x,this.pos.y);
    angleMode(DEGREES);
    ellipseMode(CENTER);
    rotate(this.dir);
    noStroke();
    fill(180);
    ellipse(0+2*this.size,0-5*this.size,12.5*this.size,50*this.size); //belly light
    fill(140);
    ellipse(0,0,12.5*this.size,60*this.size); //body dark
    ellipse(0,0+25*this.size,25*this.size,10*this.size); //tail
    ellipse(0,0,25*this.size,10*this.size); //fin
    fill(0);
    ellipse(0+2.5*this.size,0-20*this.size,2.5*this.size);// eye
    pop();
  }
  move(){
    let r = random(5,10);
    let b = new Bubble(this.pos.x,this.pos.y,r);

    if (keyIsPressed){
      start = true;
      if (!bubbleSound.isPlaying()){
      bubbleSound.play(0,1,0.2);
      }
      if (frameCount%4==1){
        bubbles.push(b);
      }
      if (keyCode===UP_ARROW){
          this.pos.y=this.pos.y-(this.speed/this.size);
          this.dir=0;
      } else if (keyCode===DOWN_ARROW){
          this.pos.y=this.pos.y+(this.speed/this.size);
          this.dir=180;
      } else if (keyCode===LEFT_ARROW){
          this.pos.x=this.pos.x-(this.speed/this.size);
          this.dir=270;
          new Bubble(this.pos.x,this.pos.y,Bubble.r);
      } else if (keyCode===RIGHT_ARROW){
        this.pos.x=this.pos.x+(this.speed/this.size);
          this.dir=90;
      }
    }
  }
}
