let x = 0;
let y = 0;
let noiseScale = 0.02;
let noiseVal;
let letters = ['T','E','T','R','I','S']
let font = ['GEORGIA','HELVETICA','CALIBRI'];
let tetris = ['E4','E4','B4','C4','D4','D4','C4','B4','A4','A4','A4','C4','E4','E4','D4','C4','B4','B4','B4','C4','D4','D4','E4','E4','C4','C4','A4','A4','A4','A4','A4','E3','E3','E3','D4','D4','F4','A5','A5','G4','F4','E4','E4','E4','C4','E4','E4','D4','C4','B4','B4','B4','C4','D4','D4','E4','E4','C4','C4','A4','A4','A4','A4','A4','A4']
let n = 0; //music notes
let angle = 0;
let angleDir = 1; //angle direction


function setup() {
  createCanvas(windowWidth, windowHeight);
  background(0);
  angleMode(DEGREES);

  monoSynth = new p5.MonoSynth();
}

function draw() {
  translate(width/2,height/2);

  for (let i=0;i<1;i++){

  /*because the noisevalue is multiplied by the x and y positions,
  letters close to each other will have a similar color*/
  noiseVal = noise(x*noiseScale,y*noiseScale);
  fill(100,noiseVal*255,100);
  rotate(angle);

  /* Rule 1: Draw letters “t, e, t, r, i, s” and
  spacing in random order and combination
  at x position. Fill() should be the noise
  value multiplied by x-position. */
  if (random(1)>1/7){
    textFont(random(font))
    text(random(letters),x,y);
  } else {
  //make space
  }

  playTune();

  /* Rule 2: Rotate x-position around the center
  of the page and increment */
  noiseScale=+0.02
  angle=angle+2*angleDir;
  x+=0.12;
  }
}




function playTune(){

  //Rule 3: Play a note in the Tetris melody for every 24th iteration.
  if(angle%24==0) {
      if(n<tetris.length){
        monoSynth.play(tetris[n]);
        n++;

      } else {
        monoSynth.play(tetris[n]);
        n=0;
        /* Rule 4: When the melody is through,
        change the direction off the rotation
        and repeat*/
        angleDir=angleDir*-1
      }
    }

}
