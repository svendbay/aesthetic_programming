[Click here](https://svendbay.gitlab.io/aesthetic_programming/miniX5/) to see my miniX5! <br>
[Click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX5/sketch.js) to see the source code! <br>
[Click here](https://gitlab.com/svendbay/aesthetic_programming/-/blob/master/miniX2/HowToAllowAudio.md) if you are having trouble hearing the music!
----

<img src="screenshot.png" width=600>

----

### Svend's miniX5

#### What are the rules in your generative program? Describe how your program performs over time? How do the rules produce emergent behavior? 
<br>

Rules:
1. Draw letters “t, e, t, r, i, s” and spacing in random order and combination at x position. Fill() should be the noise value multiplied by x-position.
2. Rotate x-position around the center of the page and increment position for every iteration.
3. Play a note in the Tetris melody for every 24th iteration. 
4. When the melody is through, change the direction off the rotation and repeat
<br>
My program draws a spiral in  which slowly expands. The line is filled in gradually changing colors. Every once in a while drawn line will change direction. The emergent behavior of the program comes from the fact that the music playing is depending on rotation of the angle, and the direction of rotation is depending on the music. 
<br>

#### What role do rules and processes have in your work? 
<br>
My program unfolds over time and gradually lets you understand what the rules of the generative program are. For instance, at first you may not realize that the spiral unfolding is actually letters clustered together, but this becomes apparent later, when they are drawn further from each other. Additionally you may not realize at first the letters drawn can spell out ’TETRIS’, but the melody playing works as a cultural reference to the game. In a sense, my program can be understood as an hermeneutic spiral. As the spiral slowly expands, so does your understanding, building on your previous interpretations of the work. 
<br>

In relation to the autonomy of the program, the uncertainty or the artists lack of control is primarily expressed in the fill() of the letters and the which letters are printed. This means that while the program will produce a very similar pattern every time it is executed, it will always be slightly different. Sometimes the program will write out the letters “t-e-t-r-i-s” in the correct combination, but statistically speaking there are over 46000 different combinations of these letters. The nature of the elements randomness in my program can be demonstrate both true randomness in the choosing of the letters, and a very deterministic sort of pseudorandomness in the noise() function.
<br>

#### Draw upon the assigned reading, how does this MiniX help you to understand the  idea of “auto-generator” (e.g. levels of control, autonomy, love and care via rules)?
<br>
The reason why I choose to reference the TETRIS game is because of this quote by Katie Salen and Eric Zimmerman in  Montfort et al.’s 10 PRINT CHR$(205.5+RND(1)); : GOTO 10:

>  “Uncertainty is a key component of meaningful play”. 
 
 I thought this was an interesting aspect of randomness role in society, which is also incapsulated in the Tetris game, where falling blocks of different shape is chosen (pseudo?)randomly. This unpredictability is what makes the game challenging and fun.
<br>

#### Do you have any further thoughts on the theme of this chapter? 
<br>
My closing thoughts on the theme of this chapter is on the relation between artist and machine, and where specifically where the artistic credibility falls. Is it the computer or the programmer that is the artist? My argument is that the coding can be seen as an artistic tool with which the artist can be express himself. But while the artist’ choice of tool does have influence of the work of art, it remains a tool chosen by the artist. For instance, if a painter chose to bring some performative indeterminacy to his/her canvas by throwing water balloons with paint at a canvas, we would not give credit to the balloons, but to the artist who chose to express him/herself through this medium. So in my opinion, while a computer program serves as an exciting and versatile medium for artistic expression as a critical-aesthetic object, the artistic credibility ultimately falls back upon its maker. 
<br>

----

#### References
<br>

- [Daniel Shiffman](https://www.youtube.com/watch?v=YcdldZ1E9gU) I.3 on perlin noise <br>

- Korobeiniki's Tetris Theme [on musicnotes.com](https://www.musicnotes.com/sheetmusic/mtd.asp?ppn=MN0227095) <br>

- Nick Montfort et al., “Randomness”, in Montfort et al, 10 PRINT CHR$(205.5+RND(1)); : GOTO 10, Cambridge, MA: MIT Press, 2012, pp. 119-146.

----


